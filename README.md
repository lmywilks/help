# README #

This README would contain all the commands for developing.

### Fix windows installer service issue ###

* sfc /scannow

### Git ###

* git remote add origin ${ repository }						- add remote
* git remote rm origin										- remove remote

### ssh-key ###

* ssh-keygen -t rsa											- generate ssh key
* pbcopy < ~/.ssh/id_rsa.pub								- copy public key

### node-gyp error (not solve) ###

* rm -r ~/.node-gyp
* npm update dependencies

### ionic 4 ###

* ionic start ${project name} blank/tabs/sidemenu --type=angular/react/vue		- create a new project 
* ionic g page pages/${page name}												- create a new page
* ionic serve -l																- run project

### Django ###

* django-admin startproject pyshop .						- create a new Django project
* python(3) manage.py runserver								- start Django project on web server
* python(3) manage.py startapp ${ app name }				- add new app for project
* python(3) manage.py makemigrations						- add new DB model for app
* python(3) manage.py migrate								- run all db migrations for project
* python(3) manage.py createsuperuser						- create super user for project

### scrapy ###

* scrapy runspider ${ file name } -o ${ target file name } 	- run scrapy script
* scrapy startproject ${ project name}						- create a new scrapy project
* scrapy list												- list all spiders
* scrapy crawl ${ spider name }								- start spider
* shub login												- scrapinghub api key
* shub deploy												- deploy project to scrapinghub


